let loginForm = document.querySelector('#login-form');
let emailAddress = document.querySelector('#staticEmail');
let passwordField = document.querySelector('#staticPassword');
let warncontainer = document.querySelector('#warnContainer');
let submitBtn = document.querySelector('.btn-lg');

loginForm.addEventListener("submit", (e) => {
    e.preventDefault()
    
    let email = emailAddress.value;
    let password = passwordField.value;
    
    // login validation

    if (email === '' && password === '') {
       emailAddress.classList.add('is-invalid');
       passwordField.classList.add('is-invalid');
       swal({
        title: "Fields cant be blank!",
        text: "Please try again",
        icon: "warning",
        })
    }else{
        submitBtn.innerHTML = 
        `
        <div class="spinner-border text-dark" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
        `

        fetch('https://capstone-academe.herokuapp.com/users/login',{
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(res => res.json()).then(data => {

            if (data){
                localStorage.setItem('id', data._id)
                localStorage.setItem('isAdmin', data.isAdmin)
               window.location.replace('./dashboard.html')
            }else{
                emailAddress.classList.add('is-invalid');
                passwordField.classList.add('is-invalid');
                warncontainer.innerHTML =
                    `
                        <div class="alert alert-danger alert-dismissible fade show float-end" role="alert">
                            <strong>Holy farkled!</strong> You entered an invalid email or password.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    `
            }
        })
    }
})