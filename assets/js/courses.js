
fetch('https://capstone-academe.herokuapp.com/courses').then(res => res.json()).then(data => {

    let courseData;


    if (data.length < 1) {
        courseData = 'No available Courses';
    }
    else {
        courseData = data.map(course => {

            let status;

            if ( course.isActive !== null) {
                if ( course.isActive === true) {

                    status = "<span class='badge rounded-pill bg-success'>Active</span>";
                }else{
                    status = "<span class='badge rounded-pill bg-danger'>Coming Soon</span>";
                }
            }
            return(
            `
            <div class="col-sm-12 col-md-4 mt-5">
               <div class="card bg-light crd">
                    <img src="${(!course.img_url) ? 
                         `
                            <div class="spinner-border text-warning" role="status">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        `
                        :
                        
                        course.img_url
                        
                    }" class="card-img card-image" alt="...">
                        <div class="card-img-overlay">
                            <h5>${course.course_name}</h5>
                            <p>${course.description}</p>
                            <p>${course.course_category}</p>
                            <p>${status}</p>
                        </div>
                </div>
            </div>        
            ` 
            )
        }).join("") 
    }

    let container = document.querySelector('#courseContainer');
    
    container.innerHTML = courseData
})

