
let regForm = document.querySelector('#regForm');
let firstName = document.querySelector('#validation-fname');
let lastName = document.querySelector('#validation-lname');
let emailField = document.querySelector('#validation-email');
let mobileNoField = document.querySelector('#mobileNo');
let password1 = document.querySelector('#validation-password');
let password2 = document.querySelector('#validation-password2');

firstName.addEventListener("input", (e) => {
    let fname = document.querySelector('#validation-fname').value;
    
    if( fname.length >= 2){
        firstName.classList.add('is-valid');
    }else{
        firstName.classList.remove('is-valid');
    }
})

lastName.addEventListener('input', (e) => {
    let lname = document.querySelector('#validation-lname').value;

    if( lname.length >= 2){
        lastName.classList.add('is-valid');
    }else{
        lastName.classList.remove('is-valid');
    }

})


emailField.addEventListener("input", (e) => {
    let regForm = document.querySelector('#regForm');
    let email = document.querySelector('#validation-email').value;
    let validContainer = regForm.querySelector('#validation-email');

    // check if email exists
    fetch('https://capstone-academe.herokuapp.com/users/emailExists',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email
        })
    }).then(res => res.json()).then(data => {
        // conditional rendering for email validation

            if (email.includes('@')) {
                if ((email != '') && (data === false) && (email.includes('.')) && (email.length > 10)){
                        validContainer.classList.remove("is-invalid");
                        validContainer.classList.add("is-valid");
                        emailValid = true;
                            
                    }else if ((email != '') || (data === true) || (email.indexOf('.com') === 0)){
                        validContainer.classList.remove("is-valid");
                        validContainer.classList.add("is-invalid");

                    }else{
                        validContainer.classList.remove("is-invalid");
                        validContainer.classList.remove("is-valid");
                        emailValid = null;
                    }
            }else{
                validContainer.classList.remove("is-valid");
                validContainer.classList.add("is-invalid");
            }
    })
})

mobileNoField.addEventListener('input', (e) => {

    let mobileNo = document.querySelector('#mobileNo').value;
    let mobile = parseInt(mobileNo);
    
    if (mobileNo.match(/^[0-9]+$/)) {
        
        if (typeof(mobile) === 'number' && mobileNo.length === 10) {
            mobileNoField.classList.remove('is-invalid');
            mobileNoField.classList.add('is-valid');
        }else if(typeof(mobile) != 'number' || mobileNo.length < 10 || mobileNo.length > 10){
            mobileNoField.classList.remove('is-valid');
            mobileNoField.classList.add('is-invalid');
        }else{
            mobileNoField.classList.remove('is-valid');
            mobileNoField.classList.remove('is-invalid');
        }

    }else if(typeof(mobile) != 'number' || mobileNo.length < 10){
        mobileNoField.classList.remove('is-valid');
        mobileNoField.classList.add('is-invalid');
    }
})

password1.addEventListener('input', (e) => {
    if (password1.value.length < 8) {
        password2.classList.remove('is-valid');
        password2.classList.add('is-invalid');
        password1.classList.remove('is-valid');
        password1.classList.add('is-invalid');
       
    }else{
        password1.classList.remove('is-invalid');
        password1.classList.add('is-valid');
    }
})

password2.addEventListener('input', (e) => {
    if (password1.value != password2.value){
        password2.classList.remove('is-valid');
        password2.classList.add('is-invalid');
    }else if (password1.value.length === 0){
        password2.classList.remove('is-valid');
        password2.classList.add('is-invalid');
    }else{
        password2.classList.add('is-valid');
        password2.classList.remove('is-invalid');
    }
})

regForm.addEventListener('submit', (e) => {
    e.preventDefault()

    let fname = document.querySelector('#validation-fname').value;
    let lname = document.querySelector('#validation-lname').value;
    let email = document.querySelector('#validation-email').value;
    let mobileNo = document.querySelector('#mobileNo').value;
    let passVal1 = document.querySelector('#validation-password').value;
    let passVal2 = document.querySelector('#validation-password2').value;

    if ((fname != '' && lname != '') && (passVal1 === passVal2) && (mobileNo.match(/^[0-9]+$/))) {

        fetch('https://capstone-academe.herokuapp.com/users/emailExists',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
            }).then(res => res.json()).then(data => {

                if (data === false) {
                    fetch('https://capstone-academe.herokuapp.com/users/register', {
                        method: 'POST',
                        headers: {
                            'Content-Type':'application/json'
                        },
                        body: JSON.stringify({
                            firstName : fname,
                            lastName : lname,   
                            email : email,
                            password : passVal1,
                            mobileNo : mobileNo
                        })
                    }).then(res => res.json()).then(data => {
                        if (data === true) {
                            swal({
                                title: "Successfully Registered",
                                text: "Please login",
                                icon: "success",
                            }).then((redirect) => {
                                if(redirect){ 
                                window.location.replace('./index.html')
                                }else{
                                    swal({
                                        title: "Something went wrong",
                                        text: "Please try again",
                                        icon: "warning",
                                    })
                                }
                            })
                        }else{
                            swal({
                                title: "Something went wrong",
                                text: "Please try again",
                                icon: "warning",
                            })
                        }
                    })
                }else{
                    swal({
                        title: "Something went wrong",
                        text: "Email already exists. Have you been here before?",
                        icon: "warning",
                        button: "Try again",
                    })
                }
            })
    }

})