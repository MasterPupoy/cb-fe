let profile = document.querySelector('#profile');
let courseNav = document.querySelector('#courses');
let settings = document.querySelector('#settings');
let adminUser = localStorage.getItem("isAdmin");
let token = localStorage.getItem('id');
let acctype = document.querySelector('#account-type')

let emailLabel = document.querySelector('#emailAdd');
let fnLabel = document.querySelector('#fnLabel');
let lnLabel = document.querySelector('#lnLabel');
let mobileLabel = document.querySelector('#mobileLabel');



profile.removeAttribute('aria-current','page');
courseNav.removeAttribute('aria-current','page');
settings.setAttribute('aria-current','page');


window.onload =() => { 
    if (!token) {
        window.location.replace('./login.html')
    }
}

if (adminUser === 'false') {

    acctype.innerHTML = `<span class="badge bg-warning text-dark fst-italic">Student</span> `
}

if (adminUser === 'true') {

    acctype.innerHTML = `<span class="badge bg-dark fst-italic">Sudo</span> `
}

document.querySelector('#logout').onclick = () => {
    fetch('https://capstone-academe.herokuapp.com/users/logout', {
        method: 'GET',
        headers: {
            Authorization: `${token}`
        }
    }).then(res => res.json()).then(passed => {
        if (passed === true) {
            localStorage.clear()
            window.location.replace('../index.html')
        }
    })
}

fetch(`https://capstone-academe.herokuapp.com/users/${token}`,{
        method: 'GET',
        headers: {
            Authorization: `${token}`
        }
    }).then(res => res.json()).then(user => {

        console.log(user)
       
        emailLabel.value = user.email;
        fnLabel.innerHTML = user.firstName;
        lnLabel.innerHTML = user.lastName;
        mobileLabel.innerHTML = user.mobileNo
    })

