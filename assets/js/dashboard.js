let content = document.querySelector('#content');
let acctype = document.querySelector('#account-type')
let modalList = document.querySelector('#modal-table');

let addContainer = document.querySelector('#add');
let addForm = document.querySelector('#addForm');

let profile = document.querySelector('#profile');
let courseNav = document.querySelector('#courses');
let settings = document.querySelector('#settings');

let adminUser = localStorage.getItem("isAdmin");
let token = localStorage.getItem('id');
let actButton;
let editButton;
let classList;
let status;

window.onload =() => { 
    if (!token) {
        window.location.replace('./login.html')
    }
}

// modify admin functionality

if (adminUser === 'false') {

    acctype.innerHTML = `<span class="badge bg-warning text-dark fst-italic">Student</span> `

    fetch(`https://capstone-academe.herokuapp.com/courses/activeCourses`,{
        }).then(res => res.json()).then(data => {

                let courseData;

                if (data.length === 0) {
                    courseData = 'No courses available';
                }else{

                    courseData = data.map(course => {
                        
                        let editButton;
                        let status = "<span class='badge rounded-pill bg-success'>Active</span>";

                        if (course.enrollees.length < 1) {
                            editButton = `<button class="btn text-white bg-warning mr-2" type="button" id="enrollButton" onclick="enroll(\'${course._id}\')" value=${course._id})">Enroll</button>`
                        }else{
                            course.enrollees.map(user => {
                                if (user.userId.includes(token)) {
                                    editButton = `<button class="btn text-white bg-success mr-2" type="button" disabled>Already Taken</button>`
                                }else if (!user.userId.includes(token)) {
                                    editButton = `<button class="btn text-white bg-warning mr-2" type="button" id="enrollButton" onclick="enroll(\'${course._id}\')" value=${course._id})">Enroll</button>`
                                }
                            })
                        }  

                        return (
                        `
                        <div class="col-sm-12 mt-5">
                        <div class="card bg-light crd">
                                    <div class="card-body">
                                        <h5>${course.course_name}</h5>
                                        <p>${course.description}</p>
                                        <p>${course.course_category}</p>
                                        <p>${status}</p>
                                        <p>₱${course.price}</p>
                                        <p>${editButton}</p>
                                    </div>
                            </div>
                        </div>        
                        `
                        );
                    })
                }
                content.innerHTML = courseData.join("") ;
            })
}

if (adminUser === 'true') {

    acctype.innerHTML = `<span class="badge bg-dark fst-italic">Sudo</span> `


    fetch(`https://capstone-academe.herokuapp.com/courses`,{
    }).then((res) => res.json()).then((data) => {
        
        let courseData;

        courseData = data.map(course => {


            if (course.isActive !== null) {

                editButton = `<button class="btn text-white bg-warning mr-2" data-bs-toggle="modal" data-bs-target="#editModal" onclick="edit(\'${course._id}\')">Edit Details</button>`;

                if ( course.isActive === true ) {
                    actButton = `<button class="btn text-white bg-dark my-1 mr-2" id="deactButton" onclick="deactivate(\'${course._id}\')">Set to inactive</button>`;
                    status = "<span class='badge rounded-pill bg-success'>Active</span>";
                    classList = `<a class="btn bg-info my-1" onclick="studentList(\'${course._id}\')" data-bs-toggle="modal" data-bs-target="#studentModal">Show Enrollees</a>`;

                }else if (course.isActive === false){
                    actButton = `<button class="btn text-white bg-success my-1" id="actButton" onclick="activate(\'${course._id}\')">Set to active</button>`;
                    status = "<span class='badge rounded-pill bg-danger'>Inactive</span>";
                    classList = ''
                }
            }
            return (
            `
            <div class="col-sm-12 mt-5">
            <div class="card bg-light crd">
                        <div class="card-body">
                            <h3>${course.course_name}</h3>
                            <p>${course.description}</p>
                            <p>${course.course_category}</p>
                            <p>${status}</p>
                            <p>₱${course.price}</p>
                            <p>${editButton}${actButton}${classList}</p>
                        </div>
                </div>
            </div>        
            `
            )
        })

        content.innerHTML = courseData.join("");
        addContainer.innerHTML = 
        `
        <div class="col-sm-12 mt-5">
            <div class="card bg-light">
                <button class="add" data-bs-toggle="modal" data-bs-target="#addModal">
                    <svg xmlns="http://www.w3.org/2000/svg" class="" width="70" height="70" fill="green" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                    <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"/>
                    </svg>
                </button>
            </div>
        </div>  
        
        `
    })
}

enroll = (courseId) => {

    let enrollButton = document.querySelector('#enrollButton');

    enrollButton.setAttribute('disabled',true);
    enrollButton.innerHTML = 
    ` 
    <div class="spinner-border text-light" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>
    `

    fetch('https://capstone-academe.herokuapp.com/users/enroll/', {
        method: "PUT",
        headers : {
            "Content-type":"application/json",
            Authorization:  `${token}`,
        },
        body: JSON.stringify({
            courseId: courseId,
        }),
    }).then((res) => {return res.json()}).then((data) => {
        console.log(data)
        if (data === true) {
            swal({
                title: "Come to thee who yearn for wisdom",
                text: "Course successfully added",
                icon: "success",
                buttons: true,
            }).then((ok) => {
                if(ok) {
                    window.location.reload();
                }else{
                swal({
                    title: "Oops!",
                    text: "Something went wrong",
                    icon: "warning",
                    })
                }   
            })
        }
    })
}

deactivate = (courseId) => {

    swal({
        title: "Deactivating Course",
        text: "\'Onto the dark side, she'll go and disappear\'",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then((deact) => {
        if (deact) {
            fetch(`https://capstone-academe.herokuapp.com/courses/${courseId}`,{
            method: 'DELETE',
            headers: {
            'Authorization' : `${token}`,
            },
            }).then(res => res.json()).then(data => {
                if (data === true) {
                window.location.reload();
                }else{
                    window.location.reload();
                }
            })
        }   
    })
}

activate = (courseId) => {

    swal({
        title: "Activate Course",
        text: "Shall we open the gates sire?",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then((act) => {
        if (act) {
            fetch(`https://capstone-academe.herokuapp.com/courses/activate/${courseId}`,{
            method: 'PATCH',
            headers: {
            'Authorization' : `${token}`,
            },
            }).then(res => res.json()).then(data => {
                if (data === true) {
                    window.location.reload();
                }else if (!data){
                    window.location.reload();
                }
            })

        }
    })
}

studentList = (courseId) => {
    fetch(`https://capstone-academe.herokuapp.com/courses/studentList/${courseId}`).then(res => res.json()).then(course => {

    course.map(student => {
        fetch(`https://capstone-academe.herokuapp.com/users/${student.userId}`).then(res => res.json()).then(data => {
         
            modalList.innerHTML +=
                    `
                    <tr>
                        <td>${data.firstName} ${data.lastName}</td>
                        <td class="pl-5">Enrolled On: ${student.enrolledOn}</td>
                    </tr>
                    `
        })
    })

    if (course.length < 1) {
        modalList.innerHTML =  
        `
        <img src="../assets/img/fox.gif" alt="fox"><span class="text-muted empty">eChooo...</span>
        `
    }
    
})
}

edit = (courseId) => {
    fetch(`https://capstone-academe.herokuapp.com/courses/${courseId}`).then(res => res.json()).then(data => {
        let nameField = document.querySelector('#courseName');
        let catField = document.querySelector('#courseCategory');
        let descField = document.querySelector('#textarea');
        let priceField = document.querySelector('#price');
        let editForm = document.querySelector('#editForm');

        nameField.placeholder = data.course_name;
        catField.value = data.course_category;
        descField.placeholder = data.description;
        priceField.placeholder = data.price;

        editForm.addEventListener("submit", (e) => {
            e.preventDefault()

            let newName = nameField.value
            let newCat = catField.value
            let newDesc = descField.value
            let newPrice = priceField.value

            fetch(`https://capstone-academe.herokuapp.com/courses/edit/${courseId}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization : `${token}`
                },
                body: JSON.stringify({
                    courseId: courseId,
                    name: newName,
                    category: newCat,
                    newDescription: newDesc,
                    price: newPrice
                })
            }).then(res => res.json()).then(data => {
                if (data === true){
                    swal({
                        title: "Success",
                        text: "Course was successfully updated",
                        icon: "success",
                        buttons: true,
                    }).then((ok) => {
                        if(ok) {
                            window.location.reload();
                        }else(
                            window.location.reload()
                        )
                    })
                }
            })
        })
    })
}

addForm.addEventListener("submit", (e) => {
    e.preventDefault()

    let courseName = document.querySelector('#addCourseName').value;
    let courseCat = document.querySelector('#addCourseCategory').value;
    let description = document.querySelector('#addTextarea').value;
    let price = document.querySelector('#addPrice').value;
    let imgPath = document.querySelector('#addUrl').value;
    let save = document.querySelector('#save');

    save.setAttribute('disabled',true);
    save.innerHTML = 
    ` 
    <div class="spinner-border text-light" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>
    `

    fetch('https://capstone-academe.herokuapp.com/courses/addCourse', {
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            Authorization: `${token}`
        },
        body: JSON.stringify({
            name: courseName,
            category: courseCat,
            description: description,
            price: price,
            imgUrl: imgPath
        })
    }).then((res) => res.json()).then(data => {
        if(data === true) {
            swal({
                title: "Come to thee who yearn for wisdom",
                text: "Course successfully added",
                icon: "success",
                buttons: true,
            }).then((ok) => {
                if(ok) {
                    window.location.reload();
                }else{
                swal({
                    title: "Oops!",
                    text: "Something went wrong",
                    icon: "warning",
                    })
                }   
            })
        }
    })
})

document.querySelector('#logout').onclick = () => {
    fetch('https://capstone-academe.herokuapp.com/users/logout', {
        method: 'GET',
        headers: {
            Authorization: `${token}`
        }
    }).then(res => res.json()).then(passed => {
        if (passed === true) {
            localStorage.clear()
            window.location.replace('../index.html')
        }
    })
}
